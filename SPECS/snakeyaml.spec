%global vertag 8450addf3473

%bcond_with spring

Name:           snakeyaml
Summary:        YAML parser and emitter for Java
Version:        1.25
Release:        3%{?dist}
License:        ASL 2.0

URL:            https://bitbucket.org/asomov/%{name}
Source0:        %{url}/get/%{name}-%{version}.tar.gz

# Upstream has forked gdata-java and base64 and refuses [1] to
# consider replacing them by external dependencies.  Bundled libraries
# need to be removed and their use replaced by system libraries.
# See rhbz#875777 and http://code.google.com/p/snakeyaml/issues/detail?id=175
#
# Remove use of bundled Base64 implementation
Patch0:         0001-Replace-bundled-base64-implementation.patch
# We don't have gdata-java in Fedora any longer, use commons-codec instead
Patch1:         0002-Replace-bundled-gdata-java-client-classes-with-commo.patch

BuildArch:      noarch

BuildRequires:  maven-local
BuildRequires:  mvn(biz.source_code:base64coder)
BuildRequires:  mvn(commons-codec:commons-codec)
BuildRequires:  mvn(commons-io:commons-io)
BuildRequires:  mvn(junit:junit)
BuildRequires:  mvn(org.apache.commons:commons-lang3)
BuildRequires:  mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-source-plugin)
BuildRequires:  mvn(org.apache.velocity:velocity)
%if %{with spring}
BuildRequires:  mvn(org.springframework:spring-core)
BuildRequires:  mvn(org.springframework:spring-beans)
BuildRequires:  mvn(org.springframework:spring-context-support)
%endif

%description
SnakeYAML features:
    * a complete YAML 1.1 parser. In particular,
      SnakeYAML can parse all examples from the specification.
    * Unicode support including UTF-8/UTF-16 input/output.
    * high-level API for serializing and deserializing
      native Java objects.
    * support for all types from the YAML types repository.
    * relatively sensible error messages.


%package        javadoc
Summary:        API documentation for %{name}

%description    javadoc
This package contains %{summary}.

%package        tests
Summary:        Tests jar for %{name}

%description    tests
This package contains %{summary}.

%prep
%setup -q -n asomov-%{name}-%{vertag}
%patch0 -p1
%patch1 -p1

%mvn_file : %{name}

%pom_remove_plugin :cobertura-maven-plugin
%pom_remove_plugin :maven-changes-plugin
%pom_remove_plugin :maven-enforcer-plugin
%pom_remove_plugin :maven-license-plugin
%pom_remove_plugin :maven-javadoc-plugin
%pom_remove_plugin :maven-site-plugin
%pom_remove_plugin :nexus-staging-maven-plugin

sed -i "/<artifactId>spring</s/spring/&-core/" pom.xml
rm -f src/test/java/examples/SpringTest.java

# Replacement for bundled gdata-java-client
%pom_add_dep commons-codec:commons-codec
# Re-add bundled base64coder
%pom_add_dep biz.source_code:base64coder

# remove bundled stuff
rm -rf target

# fails in rpmbuild only due to different locale
rm src/test/java/org/yaml/snakeyaml/issues/issue67/NonAsciiCharsInClassNameTest.java
# fails after unbundling
rm src/test/java/org/yaml/snakeyaml/issues/issue318/ContextClassLoaderTest.java

# convert CR+LF to LF
sed -i 's/\r//g' LICENSE.txt

%if %{without spring}
%pom_remove_dep org.springframework
rm -r src/test/java/org/yaml/snakeyaml/issues/issue9
rm src/test/java/org/yaml/snakeyaml/helpers/FileTestHelper.java
%endif

# Remove tests requiring joda-time
rm -rf src/test/java/examples/jodatime/
%pom_remove_dep joda-time:joda-time

%pom_xpath_inject pom:project/pom:build/pom:plugins '<plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>3.1.2</version>
        <executions>
          <execution>
            <goals>
              <goal>test-jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>'
%mvn_package org.yaml:snakeyaml:jar:tests:%{version} tests

%build
%mvn_build


%install
%mvn_install


%files -f .mfiles
%doc LICENSE.txt

%files javadoc -f .mfiles-javadoc
%doc LICENSE.txt

%files tests -f .mfiles-tests

%changelog
* Mon Oct 07 2019 Severin Gehwolf <sgehwolf@redhat.com> - 1.25-3
- Add tests sub-package.

* Mon Oct 07 2019 Severin Gehwolf <sgehwolf@redhat.com> - 1.25-2
- Initial import from Fedora.
- Remove joda-time test dependency.
